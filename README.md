# jlame

jlame library

Original port: http://dmilvdv.narod.ru/Apps/mp3_codec.html

Original code: https://sourceforge.net/projects/lame/

## Cetral Maven Repo

```xml
<dependency>
  <groupId>com.github.axet</groupId>
  <artifactId>jlame</artifactId>
  <version>3.100</version>
</dependency>
```

## Android Studio

```gradle
    api 'com.github.axet:jlame:3.100'
```
